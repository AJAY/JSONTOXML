import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import org.json.JSONException;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;

public class JSONtoXML
{
    private static String InputPath = "./json/";
    private static String OutputPath = "./xml/";

    public static void main(String[] args)
    {
        JFrame.setDefaultLookAndFeelDecorated(true);
        JDialog.setDefaultLookAndFeelDecorated(true);
        JFrame frame = new JFrame("JSON 2 XML");
        frame.setSize(200, 200);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        frame.setLayout(new FlowLayout());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JButton button = new JButton("Select File");
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                JFileChooser fileChooser = new JFileChooser();
                int returnValue = fileChooser.showOpenDialog(null);
                if (returnValue == JFileChooser.APPROVE_OPTION) {
                    File selectedFile = fileChooser.getSelectedFile();
                    InputPath += selectedFile.getName();
                    OutputPath += (selectedFile.getName().split("\\."))[0] + ".xml";
                    //Read JSON File
                    long startTime = System.currentTimeMillis();
                    String json = null;//Read File
                    try {
                        json = readFile(InputPath);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    long endTime = System.currentTimeMillis();
                    long duration = endTime - startTime;
                    System.out.println("Read File Duration: "+duration);

                    //Convert JSON to XML
                    startTime = System.currentTimeMillis();
                    String xml = null;//State name of root element tag
                    try {
                        xml = convert(json, "root");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    endTime = System.currentTimeMillis();
                    duration = endTime - startTime;
                    System.out.println("Process Data Duration: "+duration);

                    //Write XML File
                    startTime = System.currentTimeMillis();
                    try {
                        writeFile(OutputPath, xml);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    endTime = System.currentTimeMillis();
                    duration = endTime - startTime;
                    System.out.println("Write File Duration: "+duration);
                }
            }
        });
        frame.add(button);
//        frame.pack();
        frame.setVisible(true);

    }

    public static String convert(String json, String root) throws JSONException
    {
        org.json.JSONObject jsonFileObject = new org.json.JSONObject(json);
        String xml = "<?xml version=\"1.0\" encoding=\"ISO-8859-15\"?>\n<"+root+">"
                + org.json.XML.toString(jsonFileObject) + "</"+root+">";
        return xml;
    }

    public static String readFile(String filepath) throws FileNotFoundException, IOException
    {

        StringBuilder sb = new StringBuilder();
        InputStream in = new FileInputStream(InputPath);
        Charset encoding = Charset.defaultCharset();

        Reader reader = new InputStreamReader(in, encoding);

        int r = 0;
        while ((r = reader.read()) != -1)//Note! use read() rather than readLine()
        //Can process much larger files with read()
        {
            char ch = (char) r;
            sb.append(ch);
        }

        in.close();
        reader.close();

        return sb.toString();
    }

    public static void writeFile(String filepath, String output) throws IOException
    {
        FileWriter file = new FileWriter(filepath);
        BufferedWriter bf = new BufferedWriter(file);
        bf.write(output);
        bf.close();

    }
}
